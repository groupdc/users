<?php

namespace Dcms\Frontendusers;

/**
*
* @author web <web@groupdc.be>
*/
use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;

class FrontendusersServiceProvider extends ServiceProvider
{
    /**
    * Indicates if loading of the provider is deferred.
    *
    * @var bool
    */
    protected $defer = false;

    public function boot()
    {
        //update
        $this->loadViewsFrom(realpath(__DIR__.'/resources/views'), 'dcms');
        $this->setupRoutes($this->app->router);

        $this->publishes([
          __DIR__.'/config/dcms_sidebar.php' => config_path('Dcms/Frontendusers/dcms_sidebar.php'),
        ]);

        if (!is_null(config('dcms.frontendusers'))) {
            $this->app['config']['dcms_sidebar'] =  array_replace_recursive($this->app["config"]["dcms_sidebar"], config('dcms.frontendusers.dcms_sidebar'));
        }
    }
    /**
    * Define the routes for the application.
    *
    * @param  \Illuminate\Routing\Router  $router
    * @return void
    */
    public function setupRoutes(Router $router)
    {
        $router->group(['namespace' => 'Dcms\Frontendusers\Http\Controllers'], function ($router) {
            require __DIR__.'/Http/routes.php';
        });
    }

    public function register()
    {
        $this->registerFrontendUsers();
    }

    private function registerFrontendUsers()
    {
        $this->app->bind('frontendusers', function ($app) {
            return new Frontendusers($app);
        });
    }
}
