<?php

namespace Dcms\Frontendusers\Http\Controllers;

use App\Http\Controllers\Controller;
use Dcms\Frontendusers\Models\Users;
use Illuminate\Http\Request;
use View;
use Input;
use Session;
use Validator;
use Redirect;
use DB;
use Datatables;
use Auth;
use Form;
use DateTime;

class FrontendusersController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:gardenusers-browse')->only('index');
        $this->middleware('permission:gardenusers-add')->only(['create', 'store']);
        $this->middleware('permission:gardenusers-edit')->only(['edit', 'update']);
        $this->middleware('permission:gardenusers-delete')->only('destroy');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // load the view
        return View::make('dcms::frontendusers/index');
    }

    public function getDatatable()
    {
        return Datatables::queryBuilder(
            DB::connection('project')->table('customers')
                ->select(
                    'id',
                    'email',
                    'firstname',
                    'lastname',
                    'companyname',
                    'client',
                    (DB::connection("gardeneering")->raw('Concat("<img src=\'/packages/Dcms/Core/images/flag-",lcase(country),".png\' >") as country'))
                )
                ->orderBy('email')
        )
            ->addColumn('edit', function ($model) {
                $edit = '<form method="POST" action="/admin/frontendusers/' . $model->id . '" accept-charset="UTF-8" class="pull-right">
								<input name="_token" type="hidden" value="' . csrf_token() . '">
                                <input name="_method" type="hidden" value="DELETE">';
                       
                if (Auth::user()->can('gardenusers-edit')) {
                    $edit .= '<a class="btn btn-xs btn-default" href="/admin/frontendusers/' . $model->id . '/edit"><i class="far fa-pencil"></i></a>';
                }
                
                if (Auth::user()->can('gardenusers-delete')) {
                    $edit .= '<button class="btn btn-xs btn-default" type="submit" value="Delete this user" onclick="if(!confirm(\'Are you sure to delete this item?\')){return false;};"><i class="far fa-trash-alt"></i></button>';
                }
                $edit .= '</form>';
                            
                return $edit;
            })
            ->rawColumns(['country', 'edit'])
            ->make(true);
    }

    public function getExtendedModel()
    {
        //do nothing let the extend class hook into this
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $user = new Users();
        return View::make('dcms::frontendusers/form')
            ->with('user', $user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $user = Users::findOrFail($id);
        return View::make('dcms::frontendusers/form')
            ->with('user', $user);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        if ($this->validateUserForm($request) === true) {
            $user = new Users();
            if ($this->emailExists($request->email) === false) {
                $user = $this->processUserForm($user, $request);

                // redirect
                Session::flash('message', 'Successfully created user!');
                return redirect()->route('admin.frontendusers.index');
            } else {
                return $this->emailExists($request->email);
            }
        } else {
            return $this->validateUserForm($request);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update(Request $request, $id)
    {
        if ($this->validateUserForm($request) === true) {
            $user = Users::findOrFail($id);

            if (strtolower($user->email) <> trim(strtolower($request->email))) {
                if ($this->emailExists($request->email) !== false) {
                    return $this->emailExists($request->email);
                }
            }

            $user = $this->processUserForm($user, $request);

            // redirect
            Session::flash('message', 'Successfully updated user!');
            return redirect()->route('admin.frontendusers.index');
        } else {
            return $this->validateUserForm($request);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        Users::where('id', $id)->delete();
        return Redirect::to('admin/frontendusers');
    }

    private function emailExists($email = null)
    {
        if (Users::where('email', '=', $email)->exists()) {
            return Redirect::back()->withErrors(['email'=>'This email adress is allready used by another account'])->withInput();
        }
        return false;
    }

    private function validateUserForm(Request $request)
    {
        $rules = [
            'email' => 'required|email'
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        } else {
            return true;
        }
    }

    private function processUserForm($user, $request)
    {
        $user->country = $request->country;
        $user->language = $request->language;
        $user->email = $request->email;
        $user->companyname = $request->get('companyname');
        $user->client = $request->get('client');
        $user->firstname = $request->get('firstname');
        $user->lastname = $request->get('lastname');
        $user->comment = $request->get('code');
        $user->retailapproved = ($request->get('retailapproved')==1 ? 1 : 0);

        $user->save();

        return $user;
    }
}
