<?php

Route::group(['middleware' => ['web']], function () {

	Route::group( array("prefix" => "admin", "as"=>"admin."), function() {

    	Route::group(['middleware' => 'auth:dcms'], function() {

			//Advices
			Route::group( array("prefix" => "frontendusers" ,"as"=>"frontendusers."), function() {
      			//API
      			Route::group( array("prefix" => "api", "as"=>"api."), function() {
      				Route::any('table', array('as'=>'table', 'uses' => 'FrontendusersController@getDatatable'));
      			});
      		});

      		Route::resource('frontendusers','FrontendusersController');
    });
  });
});

 ?>
