<?php

namespace Dcms\Frontendusers\Models;

use Dcms\Core\Models\EloquentDefaults;

class Users extends EloquentDefaults
{
    protected $connection = 'project';
    protected $table = 'customers';
}
