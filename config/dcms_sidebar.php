<?php

return ["Users"=> [ 
            "icon" => "fa-users",
            "links" => [
                [
                    "route" => "admin/frontendusers", 
                    "label" => "Users", 
                    "permission" => 'gardenusers'
                ],
            ]
        ]
];

?>
