@extends("dcms::template/layout")

@section("content")

    <div class="main-header">
        <h1>Garden users</h1>
        <ol class="breadcrumb">
            <li><a href="{!! URL::to('admin/dashboard') !!}"><i class="far fa-tachometer-alt-average"></i> Dashboard</a></li>
            <li><a href="{!! URL::to('admin/frontendusers') !!}"><i class="far fa-pencil"></i> Garden users</a></li>
            @if($user->exists)
                <li class="active">Edit</li>
            @else
                <li class="active">Create</li>
            @endif
        </ol>
    </div>

    <div class="main-content">
        @if($user->exists)
        <form action="{{ route('admin.frontendusers.update', $user->id) }}" method="post">
            {{ method_field('PUT') }}
        @else
        <form action="{{ route('admin.frontendusers.store') }}" method="post">
        @endif
        {!! csrf_field() !!}
        <div class="row">
            <div class="col-md-9">
                <div class="main-content-tab tab-container">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active"><a href="#data" role="tab" data-toggle="tab">Data</a></li>
                        @yield('extratabs')
                    </ul>

                    <div class="tab-content">
                        <!-- Data -->
                        <div id="data" class="tab-pane active">
                            <div class="tab-content">

                                <div class="form-group">
                                    <label for="email">E-mail</label>
                                    <input type="text" class="form-control" id="email" name="email" value="{{ $user->email }}">
                                </div>

                                <div class="form-group">
                                    <label for="lastname">Lastname</label>
                                    <input type="text" class="form-control" id="lastname" name="lastname" value="{{ $user->lastname }}">
                                </div>

                                <div class="form-group">
                                    <label for="firstname">Firstname</label>
                                    <input type="text" class="form-control" id="firstname" name="firstname" value="{{ $user->firstname }}">
                                </div>

                                <div class="form-group">
                                    <label for="email">Country</label>
                                    <select name="country">
                                        @foreach(['BE','NL','LU','DE','FR'] as $country)
                                            <option value="{{ $country }}" @if($user->country == $country) selected @endif >{{ $country }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="email">Language</label>
                                    <select name="language">
                                        @foreach(['nl','fr','de'] as $language)
                                            <option value="{{ $language }}" @if($user->language == $language) selected @endif >{{ $language }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="retail">Retail enabled</label>
                                    <input type="checkbox" class="" id="retailapproved" name="retailapproved" value="1" @if($user->retailapproved == "1") checked @endif>
                                </div>

                                <div class="form-group">
                                    <label for="companyname">Company</label>
                                    <input type="text" class="form-control" id="companyname" name="companyname" value="{{ $user->companyname }}">
                                </div>

                                <div class="form-group">
                                    <label for="code">Code (SAP)</label>
                                    <input type="text" class="form-control" id="code" name="code" value="{{ $user->comment }}">
                                </div>

                                <div class="form-group">
                                    <input type="checkbox" name="client" id="client" value="1" class="form-checkbox" {{ $user->client ? 'checked' : '' }}>
                                    <label for="client" class="checkbox {{ $user->client ? 'active' : '' }}">Client</label>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="main-content-block">
                    {!! Form::submit('Save', array('class' => 'btn btn-primary')) !!}
                    <a href="{!! URL::previous() !!}" class="btn btn-default">Cancel</a>
                </div>
            </div>
        </div>
        </form>
    </div>

@stop

@section("script")
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/plug-ins/be7019ee387/integration/bootstrap/3/dataTables.bootstrap.css">

    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/plug-ins/be7019ee387/integration/bootstrap/3/dataTables.bootstrap.js"></script>

    <script type="text/javascript" src="{!! asset('/packages/Dcms/Core/js/bootstrap.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('/packages/Dcms/Core/ckeditor/ckeditor.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('/packages/Dcms/Core/ckeditor/adapters/jquery.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('/packages/Dcms/Core//ckfinder/ckfinder.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('/packages/Dcms/Core//ckfinder/ckbrowser.js') !!}"></script>

    <script type="text/javascript">
        $(document).ready(function () {

            //CKFinder for CKEditor
            CKFinder.setupCKEditor(null, '/packages/Dcms/Core/ckfinder/');

            //CKEditor
            $("textarea[id='description']").ckeditor();
            $("textarea[id='body']").ckeditor();

            //Bootstrap Tabs
            $(".tab-container .nav-tabs a").click(function (e) {
                e.preventDefault();
                $(this).tab('show');
            });
        });
    </script>

@stop
